﻿using OnlineMedic.Data.DataTransport;
using OnlineMedic.Data.Infrastructure;
using OnlineMedic.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Repositories
{
    public class DoctorRepository : RepositoryBase<Doctor, string>, IDoctorRepository
    {
        public DoctorRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public void AddRating(Rating rating)
        {
            DbContext.Ratings.Add(rating);
        }

        public IEnumerable<Category> GetCategories()
        {
            return DbContext.Categories.ToList();
        }

        public IEnumerable<DoctorShort> GetDoctorsShort(int? categoryId = default(int?), float? minPrice = default(float?), float? maxPrice = default(float?), double? maxDistance = default(double?), double latitude = 0, double longitude = 0)
        {
            var result = from d in DbContext.Doctors
                         select new
                         {
                             FullName = d.FullName,
                             Category = d.Category.Name,
                             CategoryId = d.Category.Id,
                             Ratings = d.Ratings.Select(r => r.Score).DefaultIfEmpty(0).Average(),
                             UserId = d.UserId,
                             ServicePrice = d.ServicePrice,
                             Latitude = d.Latitude,
                             Longitude = d.Longitude
                         };

            if (categoryId != null && categoryId != 0)
            {
                result = result.Where(r => r.CategoryId == categoryId);
            }
            if (minPrice != null)
            {
                result = result.Where(r => r.ServicePrice >= minPrice);
            }
            if (maxPrice != null)
            {
                result = result.Where(r => r.ServicePrice <= maxPrice);
            }

            var resultList = result.ToList();



            if (maxDistance != null)
            {
                resultList = resultList.Where(r => CalculateDistance(r.Latitude, r.Longitude, latitude, longitude) <= maxDistance).ToList();
            }

            return resultList.Select(d => new DoctorShort
            {
                FullName = d.FullName,
                Category = d.Category,
                Ratings = d.Ratings,
                UserId = d.UserId,
                ServicePrice = d.ServicePrice
            }).ToList();
        }

        private double CalculateDistance(double lat1, double lon1, double lat2, double lon2)
        {
            var sCoord = new GeoCoordinate(lat1, lon1);
            var eCoord = new GeoCoordinate(lat2, lon2);
            
            return sCoord.GetDistanceTo(eCoord)/1000;
        }
    }

    public interface IDoctorRepository : IRepository<Doctor, string>
    {
        IEnumerable<DoctorShort> GetDoctorsShort(int? categoryId = null, float? minPrice = null, float? maxPrice = null, double? maxDistance = null, double latitude = 0, double longitude = 0);
        IEnumerable<Category> GetCategories();
        void AddRating(Rating rating);
    }
}
