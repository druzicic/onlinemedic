﻿using OnlineMedic.Data.Infrastructure;
using OnlineMedic.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Repositories
{
    public class CalendarRepository : RepositoryBase<Calendar, int>, ICalendarRepository
    {
        public CalendarRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public bool CancelReservedDoctor(DateTime dateTime, string doctorId)
        {
            var selectedEntry = DbContext.Calendar.Where(c => doctorId == c.DoctorId && dateTime == c.Date).FirstOrDefault();

            if (selectedEntry == null)
            {
                return false;
            }
            if (selectedEntry.Free == true || string.IsNullOrEmpty(selectedEntry.PatientId) == true)
            {
                return false;
            }

            selectedEntry.Free = true;
            selectedEntry.Confirmed = false;
            selectedEntry.PatientId = null;

            DbContext.SaveChanges();

            return true;
        }

        public bool ConfirmEntryDoctor(DateTime dateTime, string doctorId)
        {
            var selectedEntry = DbContext.Calendar.Where(c => doctorId == c.DoctorId && dateTime == c.Date).FirstOrDefault();

            if (selectedEntry == null)
            {
                return false;
            }
            if (selectedEntry.Free == true || selectedEntry.Confirmed == true || string.IsNullOrEmpty(selectedEntry.PatientId) == true)
            {
                return false;
            }

            selectedEntry.Free = false;
            selectedEntry.Confirmed = true;

            DbContext.SaveChanges();

            return true;
        }

        public bool InvertFreeEntry(DateTime dateTime, string doctorId)
        {
            var selectedEntry = DbContext.Calendar.Where(c => doctorId == c.DoctorId && dateTime == c.Date).FirstOrDefault();
            
            if (selectedEntry == null)
            {
                return false;
            }
            if (selectedEntry.Free == false)
            {
                return false;
            }

            DbContext.Calendar.Remove(selectedEntry);
            DbContext.SaveChanges();

            return true;
        }

        public IEnumerable<Calendar> GetDoctorsCalendar(string doctorId, DateTime date)
        {
            var date2 = date.AddDays(7);
            return DbContext.Calendar.Include("Patient").Where(c => doctorId == c.DoctorId).Where(c => c.Date > date && c.Date < date2).ToList();     
        }

        public bool RejectEntryDoctor(DateTime dateTime, string doctorId)
        {
            var selectedEntry = DbContext.Calendar.Where(c => doctorId == c.DoctorId && dateTime == c.Date).FirstOrDefault();

            if (selectedEntry == null)
            {
                return false;
            }
            if (selectedEntry.Free == true || selectedEntry.Confirmed == true || string.IsNullOrEmpty(selectedEntry.PatientId) == true)
            {
                return false;
            }

            selectedEntry.Free = true;
            selectedEntry.Confirmed = false;
            selectedEntry.PatientId = null;

            DbContext.SaveChanges();

            return true;
        }

        public bool ReserveEntry(DateTime dateTime, string doctorId, string patientId)
        {
            var selectedEntry = DbContext.Calendar.Where(c => doctorId == c.DoctorId && dateTime == c.Date).FirstOrDefault();

            if (selectedEntry == null)
            {
                return false;
            }
            if (selectedEntry.Free == false)
            {
                return false;
            }

            selectedEntry.Free = false;
            selectedEntry.Confirmed = false;
            selectedEntry.PatientId = patientId;

            DbContext.SaveChanges();

            return true;
        }

        public Calendar GetEntry(DateTime dateTime, string doctorId)
        {
            var selectedEntry = DbContext.Calendar.Where(c => doctorId == c.DoctorId && dateTime == c.Date).FirstOrDefault();

            return selectedEntry;
        }

        public IEnumerable<Calendar> GetPatientsCalendar(string patientId, DateTime date)
        {
            var date2 = date.AddDays(7);
            return DbContext.Calendar.Include("Doctor").Where(c => patientId == c.PatientId).Where(c => c.Date > date && c.Date < date2).ToList();
        }
    }

    public interface ICalendarRepository : IRepository<Calendar, int>
    {
        IEnumerable<Calendar> GetDoctorsCalendar(string doctorId, DateTime date);
        IEnumerable<Calendar> GetPatientsCalendar(string patientId, DateTime date);
        bool InvertFreeEntry(DateTime dateTime, string doctorId);

        bool ReserveEntry(DateTime dateTime, string doctorId, string patientId);
        bool RejectEntryDoctor(DateTime dateTime, string doctorId);
        bool ConfirmEntryDoctor(DateTime dateTime, string doctorId);
        bool CancelReservedDoctor(DateTime dateTime, string doctorId);
        Calendar GetEntry(DateTime dateTime, string doctorId);
    }
}
