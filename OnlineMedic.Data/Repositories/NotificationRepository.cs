﻿using OnlineMedic.Data.Infrastructure;
using OnlineMedic.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Repositories
{

    public class NotificationRepository : RepositoryBase<Notification, int>, INotificationRepository
    {
        public NotificationRepository(IDbFactory dbFactory)
            : base(dbFactory) { }


    }

    public interface INotificationRepository : IRepository<Notification, int>
    {

    }
}
