﻿using OnlineMedic.Data.DataTransport;
using OnlineMedic.Data.Infrastructure;
using OnlineMedic.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Repositories
{
    public class RecordRepository : RepositoryBase<Record, int>, IRecordRepository
    {
        public RecordRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public void AddRecordFile(RecordFile file)
        {
            DbContext.RecordFiles.Add(file);
        }

        public Record GetFullRecord(int id)
        {
            return DbContext.Records.Where(r => r.Id == id).Include(r => r.Files).FirstOrDefault();
        }

        public IEnumerable<RecordShort> GetPatientRecordsShortInfo(string id)
        {
            var result = from r in DbContext.Records
                         join d in DbContext.Doctors on r.DoctorId equals d.UserId
                         select new RecordShort
                         {
                             Id = r.Id,
                             Date = r.Date,
                             DoctorId = r.DoctorId,
                             PatientId = r.PatientId,
                             DoctorName = d.FullName,
                             Title = r.Title
                         };
            return result.ToList();
        }
    }

    public interface IRecordRepository : IRepository<Record, int>
    {
        IEnumerable<RecordShort> GetPatientRecordsShortInfo(string id);
        Record GetFullRecord(int id);

        void AddRecordFile(RecordFile file);
    }
}
