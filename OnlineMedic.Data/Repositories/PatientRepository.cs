﻿using OnlineMedic.Data.Infrastructure;
using OnlineMedic.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Repositories
{
    public class PatientRepository : RepositoryBase<Patient, string>, IPatientRepository
    {
        public PatientRepository(IDbFactory dbFactory)
            : base(dbFactory) { }


    }

    public interface IPatientRepository : IRepository<Patient, string>
    {

    }
}
