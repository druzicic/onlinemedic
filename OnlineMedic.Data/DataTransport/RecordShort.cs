﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.DataTransport
{
    public class RecordShort
    {
        public int Id { get; set; }
        public string DoctorId { get; set; }
        public string PatientId { get; set; }
        public string DoctorName { get; set;}
        public DateTime Date { get; set; }
        public string Title { get; set; }
    }
}
