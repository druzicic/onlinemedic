﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.DataTransport
{
    public class DoctorShort
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public float ServicePrice { get; set; }
        public double Ratings { get; set; }
        public string Category { get; set; }
    }
}
