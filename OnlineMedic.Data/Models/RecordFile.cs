﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Models
{
    public class RecordFile
    {
        public int Id { get; set; }

        [Required]
        public int RecordId { get; set; }

        [Required]
        public string FileLocation { get; set; }
    }
}
