﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Models
{
    public class Record
    {
        public int Id { get; set; }

        [Required]
        [ForeignKey("Doctor")]
        public string DoctorId { get; set; }

        public virtual Doctor Doctor { get; set; }

        [Required]
        [ForeignKey("Patient")]
        public string PatientId { get; set; }

        public virtual Patient Patient { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Text { get; set; }

        public virtual ICollection<RecordFile> Files { get; set; }
    }
}
