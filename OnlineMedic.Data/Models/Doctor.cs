﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Models
{
    public class Doctor
    {
        [Key]
        public string UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string FullName { get; set; }

        [Required]
        public float ServicePrice { get; set; }

        [Required]
        public string Biography { get; set; }

        [Required]
        public double Latitude { get; set; }

        [Required]
        public double Longitude { get; set; }

        [Required]
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<Record> Records { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Flag> Flags { get; set; }        
    }
}
