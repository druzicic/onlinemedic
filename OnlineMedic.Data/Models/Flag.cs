﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Models
{
    public class Flag
    {
        public int Id { get; set; }

        [Required]
        [ForeignKey("Doctor")]
        public string DoctorId { get; set; }

        public virtual Doctor Doctor { get; set; }

        [Required]
        [ForeignKey("Patient")]
        public string PatientId { get; set; }

        public virtual Patient Patient { get; set; }

        [Required]
        public string Comment { get; set; }
    }
}
