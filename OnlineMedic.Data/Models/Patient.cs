﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedic.Data.Models
{
    public class Patient
    {
        [Key]
        public string UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string FullName { get; set; }

        public virtual ICollection<Record> Records { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Flag> Flags { get; set; }
    }
}
