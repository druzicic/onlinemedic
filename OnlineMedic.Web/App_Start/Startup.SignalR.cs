﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web
{
    public partial class Startup
    {
        public void ConfigureSignalR(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}