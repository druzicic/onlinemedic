﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OnlineMedic.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "PatientsMyRecord",
                url: "Patients/MyRecord/{recordid}",
                defaults: new { controller = "Patients", action = "MyRecord", recordid = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "PatientsErecord",
                url: "Patients/{patientId}/{action}/{recordid}",
                defaults: new { controller = "Patients", action = "Index", recordid = UrlParameter.Optional  }
            );

            routes.MapRoute(
                name: "DoctorAddRating",
                url: "Doctors/Details/{docId}/AddRating",
                defaults: new { controller = "Doctors", action = "AddRating" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
