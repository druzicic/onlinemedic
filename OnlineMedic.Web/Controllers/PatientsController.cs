﻿using Microsoft.AspNet.Identity;
using OnlineMedic.Data.DataTransport;
using OnlineMedic.Data.Models;
using OnlineMedic.Web.BusinessLogic;
using OnlineMedic.Web.Filters;
using OnlineMedic.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineMedic.Web.Controllers
{
    [Authorize]
    public class PatientsController : Controller
    {
        private readonly IPatientLogic _patientLogic;
        private readonly IMedicalExaminationLogic _medicalExaminationLogic;

        public PatientsController(IPatientLogic patientLogic, IMedicalExaminationLogic medicalLogic)
        {
            _patientLogic = patientLogic;
            _medicalExaminationLogic = medicalLogic;
        }
        // GET: Patients

        [RolesAuthorizationFilter(Roles = "Doctor")]
        public ActionResult Index()
        {
            var patients = _patientLogic.GetAll();
            var patientsViewModel = AutoMapper.Mapper.Map<IEnumerable<Patient>, IEnumerable<PatientListViewModel>>(patients);

            return View(patientsViewModel);
        }

        [RolesAuthorizationFilter(Roles = "Doctor")]
        public ActionResult AllRecords(string patientId)
        {
            var records = _medicalExaminationLogic.GetAllRecordsFromPatientShort(patientId);
            var recordsViewModel = AutoMapper.Mapper.Map<IEnumerable<RecordShort>, IEnumerable<RecordListViewModel>>(records);
            return View(recordsViewModel);
        }

        [RolesAuthorizationFilter(Roles = "Doctor")]
        public ActionResult Records(string patientId, int recordId)
        {
            var record = _medicalExaminationLogic.GetRecord(recordId);
            var recordViewModel = AutoMapper.Mapper.Map<Record, RecordViewModel>(record);
            return View(recordViewModel);
        }

        [RolesAuthorizationFilter(Roles = "Doctor")]
        public ActionResult AddRecord(string patientId)
        {
            return View();
        }

        [RolesAuthorizationFilter(Roles = "Doctor")]
        [HttpPost]
        public ActionResult AddRecord(string patientId, RecordCreateViewModel record)
        {
            if (ModelState.IsValid)
            {
                var recordData = new Record
                {
                    Date = DateTime.Now,
                    Title = record.Title,
                    Text = record.Text,
                    DoctorId = User.Identity.GetUserId(),
                    PatientId = patientId
                };
                _medicalExaminationLogic.AddRecord(recordData);
                _medicalExaminationLogic.SaveChanges();

                string folderPath = Server.MapPath("~/Documents/" + recordData.Id);
                if (Directory.Exists(folderPath) == false)
                {
                    Directory.CreateDirectory(folderPath);
                }
                foreach (string upload in Request.Files)
                {
                    if (Request.Files[upload].ContentLength == 0) continue;
                    string pathToSave = Server.MapPath("~/Documents/" + recordData.Id);
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));

                    var recordFile = new RecordFile { RecordId = recordData.Id, FileLocation = Path.Combine(pathToSave, filename) };
                    _medicalExaminationLogic.AddRecordFile(recordFile);
                }
                _medicalExaminationLogic.SaveChanges();

                return RedirectToAction("AllRecords", new { patientId = patientId });
            }

            return View(record);
        }

        [RolesAuthorizationFilter(Roles = "Patient")]
        public ActionResult MyRecord()
        {
            var records = _medicalExaminationLogic.GetAllRecordsFromPatientShort(User.Identity.GetUserId());
            var recordsViewModel = AutoMapper.Mapper.Map<IEnumerable<RecordShort>, IEnumerable<RecordListViewModel>>(records);
            return View("AllRecords", recordsViewModel);
        }
    }
}