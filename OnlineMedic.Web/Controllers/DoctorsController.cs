﻿using Microsoft.AspNet.Identity;
using OnlineMedic.Data.DataTransport;
using OnlineMedic.Data.Models;
using OnlineMedic.Web.BusinessLogic;
using OnlineMedic.Web.Filters;
using OnlineMedic.Web.Models;
using OnlineMedic.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineMedic.Web.Controllers
{
    [Authorize]
    public class DoctorsController : Controller
    {
        private readonly IDoctorLogic _doctorLogic;

        public DoctorsController(IDoctorLogic doctorLogic)
        {
            _doctorLogic = doctorLogic;
        }

        // GET: Doctors
        public ActionResult Index(int? categoryId, float? minPrice, float? maxPrice, double? maxDistance, double? latitude, double? longitude)
        {
            double selectedLatitude = 0;
            double selectedLongitude = 0;
            if (latitude != null)
            {
                selectedLatitude = (double)latitude;
            }
            if (longitude != null)
            {
                selectedLongitude = (double)longitude;
            }

            var doctors = _doctorLogic.GetDoctorsShort(categoryId, minPrice, maxPrice, maxDistance, selectedLatitude, selectedLongitude);
            var doctorsViewModel = AutoMapper.Mapper.Map<IEnumerable<DoctorShort>, IEnumerable<DoctorListViewModel>>(doctors);

            var categories = _doctorLogic.GetCategories();
            var selectList = categories.Select(c => new SelectListItem
            {
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();
            selectList.Insert(0, new SelectListItem
            {
                Text = "Svi",
                Value = "0"
            });

            ViewBag.Categories = new SelectList(selectList, "Value", "Text", categoryId != null ? categoryId.ToString() :  selectList.First().Value);

            ViewBag.SearchData = new DoctorSearchViewModel
            {
                MinPrice = minPrice,
                MaxPrice = maxPrice,
                MaxDistance = maxDistance,
                Latitude = latitude,
                Longitude = longitude
            };

            return View(doctorsViewModel);
        }

        // GET: Doctors/Details/5
        public ActionResult Details(string id)
        {
            var doctor = _doctorLogic.GetById(id);
            //var doctorViewModel = AutoMapper.Mapper.Map<Doctor, DoctorViewModel>(doctor);

            if (doctor == null)
            {
                return new ViewResult() { ViewName = "Error_403" };
            }

            var doctorViewModel = new DoctorDetailsViewModel
            {
                FullName = doctor.FullName,
                Biography = doctor.Biography,
                Latitude = doctor.Latitude,
                Longitude = doctor.Longitude,
                Category = doctor.Category.Name,
                ServicePrice = doctor.ServicePrice,
                UserId = doctor.UserId,
                Ratings = doctor.Ratings.Select(r => r.Score).DefaultIfEmpty(0).Average(),
                Comments = doctor.Ratings.Select(r => r.Comment).ToList()
            };

            return View(doctorViewModel);
        }

        [RolesAuthorizationFilter(Roles = "Patient")]
        public ActionResult AddRating(string docId)
        {
            return View();
        }

        [HttpPost]
        [RolesAuthorizationFilter(Roles = "Patient")]
        public ActionResult AddRating(string docId, RatingViewModel rating)
        {
            if (ModelState.IsValid)
            {
                Rating ratingModel = new Rating
                {
                    DoctorId = docId,
                    PatientId = User.Identity.GetUserId(),
                    Score = rating.Score,
                    Comment = rating.Comment
                };
                _doctorLogic.AddRating(ratingModel);
                _doctorLogic.SaveChanges();

                return RedirectToAction("Details", "Doctors", new { id = docId });
            }

            return View(rating);
        }

        [RolesAuthorizationFilter(Roles = "Doctor")]
        public ActionResult MyProfile()
        {
            var doctor = _doctorLogic.GetById(User.Identity.GetUserId());
            var doctorViewModel = AutoMapper.Mapper.Map<Doctor, DoctorViewModel>(doctor);

            var categories = _doctorLogic.GetCategories();
            var selectList = categories.Select(c => new SelectListItem
            {
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();

            ViewBag.Categories = new SelectList(selectList, "Value", "Text", doctorViewModel.CategoryId.ToString());

            return View(doctorViewModel);
        }

        [RolesAuthorizationFilter(Roles = "Doctor")]
        [HttpPost]
        public ActionResult MyProfile(DoctorViewModel doctor)
        {
            if (ModelState.IsValid)
            {
                Doctor doctorModel = new Doctor
                {
                    UserId = User.Identity.GetUserId(),
                    Biography = doctor.Biography,
                    CategoryId = doctor.CategoryId,
                    FullName = doctor.FullName,
                    Latitude = doctor.Latitude,
                    Longitude = doctor.Longitude,
                    ServicePrice = doctor.ServicePrice
                };
                _doctorLogic.Update(doctorModel);
                _doctorLogic.SaveChanges();

                return RedirectToAction("Index", "Home");
            }

            var categories = _doctorLogic.GetCategories();
            var selectList = categories.Select(c => new SelectListItem
            {
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();

            ViewBag.Categories = new SelectList(selectList, "Value", "Text", doctor.CategoryId.ToString());

            return View(doctor);
        }
    }
}
