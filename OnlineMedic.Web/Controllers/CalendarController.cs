﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using OnlineMedic.Data.Models;
using OnlineMedic.Web.BusinessLogic;
using OnlineMedic.Web.Hubs;
using OnlineMedic.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineMedic.Web.Controllers
{
    public class CalendarController : Controller
    {
        private readonly ICalendarLogic _calendarLogic;
        private readonly IPatientLogic _patientLogic;

        public CalendarController(ICalendarLogic cl, IPatientLogic pl)
        {
            _calendarLogic = cl;
            _patientLogic = pl;
        }

        #region View Actions
        // GET: Calendar
        public ActionResult Doctor(string id, string date)
        {
            //Select start date
            DateTime startDate;
            if (string.IsNullOrEmpty(date))
            {
                startDate = DateTime.Now;
            }
            else
            {
                startDate = DateTime.Parse(date);

                startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 1, 0, 0);
                var nowDateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
                if (startDate < nowDateTemp)
                {
                    startDate = DateTime.Now;
                }
            }
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 1, 0, 0);

            //Generate days names
            List<string> daysNameHtmlList = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                var tempDate = startDate.AddDays(i);
                string dayString = $"<th class='text-center'><div>{tempDate.ToShortDateString()}</div><div>{tempDate.DayOfWeek.ToString()}</div></th>";
                daysNameHtmlList.Add(dayString);
            }
            ViewBag.DaysNames = daysNameHtmlList;


            //Get entries form DB and create List with entries info
            var calendarEntriesDb = _calendarLogic.GetDoctorsCalendar(id, startDate);
            List<List<CalendarEntryViewModel>> calendarEntriesList = new List<List<CalendarEntryViewModel>>();
            for (int j = 0; j < 14; j++)
            {
                List<CalendarEntryViewModel> tempList = new List<CalendarEntryViewModel>();                
                for (int i = 0; i < 7; i++)
                {
                    var tempDate = startDate.AddDays(i);
                    var dateToCompare = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 8 + j, 0, 0);
                    var calEntry = new CalendarEntryViewModel
                    {
                        Time = (8 + j).ToString(),
                        Status = "R",
                        Date = tempDate.ToShortDateString()
                    };

                    foreach (var entryFromDb in calendarEntriesDb)
                    {
                        if (dateToCompare.Ticks == entryFromDb.Date.Ticks)
                        {
                            if (entryFromDb.PatientId == User.Identity.GetUserId())
                            {
                                calEntry.Status = "M";
                                if (entryFromDb.Confirmed == false)
                                {
                                    calEntry.Status = "P";
                                }
                            }
                            else
                            {
                                calEntry.Status = "F";
                            }
                        }
                    }
                    tempList.Add(calEntry);
                }
                calendarEntriesList.Add(tempList);
            }

            ViewBag.CalendarEntries = calendarEntriesList;
            ViewBag.DoctorId = id;
            ViewBag.PatientId = User.Identity.GetUserId();

            var prevDate = startDate.AddDays(-7);
            prevDate = new DateTime(prevDate.Year, prevDate.Month, prevDate.Day, 1, 0, 0);
            var nowDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
            if (prevDate < nowDate)
            {
                prevDate = DateTime.Now;
            }
            var nextDate = startDate.AddDays(7);

            ViewBag.PrevDate = prevDate.ToShortDateString();
            ViewBag.NextDate = nextDate.ToShortDateString();

            return View();
        }

        public ActionResult MyCalendarDoctor(string date)
        {
            //Select start date
            DateTime startDate;
            if (string.IsNullOrEmpty(date))
            {
                startDate = DateTime.Now;
            }
            else
            {
                startDate = DateTime.Parse(date);

                startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 1, 0, 0);
                var nowDateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
                if (startDate < nowDateTemp)
                {
                    startDate = DateTime.Now;
                }
            }
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 1, 0, 0);

            //Generate days names
            List<string> daysNameHtmlList = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                var tempDate = startDate.AddDays(i);
                string dayString = $"<th class='text-center'><div>{tempDate.ToShortDateString()}</div><div>{tempDate.DayOfWeek.ToString()}</div></th>";
                daysNameHtmlList.Add(dayString);
            }
            ViewBag.DaysNames = daysNameHtmlList;


            //Get entries form DB and create List with entries info
            var calendarEntriesDb = _calendarLogic.GetDoctorsCalendar(User.Identity.GetUserId(), startDate);
            List<List<CalendarEntryViewModel>> calendarEntriesList = new List<List<CalendarEntryViewModel>>();
            for (int j = 0; j < 14; j++)
            {
                List<CalendarEntryViewModel> tempList = new List<CalendarEntryViewModel>();
                for (int i = 0; i < 7; i++)
                {
                    var tempDate = startDate.AddDays(i);
                    var dateToCompare = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 8 + j, 0, 0);
                    var calEntry = new CalendarEntryViewModel
                    {
                        Time = (8 + j).ToString(),
                        Status = "E",
                        Date = tempDate.ToShortDateString()
                    };

                    foreach (var entryFromDb in calendarEntriesDb)
                    {
                        if (dateToCompare.Ticks == entryFromDb.Date.Ticks)
                        {
                            if (entryFromDb.Free == true)
                            {
                                calEntry.Status = "F";
                            }
                            else
                            {
                                calEntry.Status = "R";
                                calEntry.Name = entryFromDb.Patient.FullName;
                                calEntry.UserId = entryFromDb.PatientId;
                                if (entryFromDb.Confirmed == false)
                                {
                                    calEntry.Status = "P";
                                }
                            }
                        }
                    }
                    tempList.Add(calEntry);
                }
                calendarEntriesList.Add(tempList);
            }

            ViewBag.CalendarEntries = calendarEntriesList;
            ViewBag.DoctorId = User.Identity.GetUserId();

            var prevDate = startDate.AddDays(-7);
            prevDate = new DateTime(prevDate.Year, prevDate.Month, prevDate.Day, 1, 0, 0);
            var nowDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
            if (prevDate < nowDate)
            {
                prevDate = DateTime.Now;
            }
            var nextDate = startDate.AddDays(7);

            ViewBag.PrevDate = prevDate.ToShortDateString();
            ViewBag.NextDate = nextDate.ToShortDateString();

            return View();
        }

        public ActionResult MyCalendarPatient(string date)
        {
            //Select start date
            DateTime startDate;
            if (string.IsNullOrEmpty(date))
            {
                startDate = DateTime.Now;
            }
            else
            {
                startDate = DateTime.Parse(date);

                startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 1, 0, 0);
                var nowDateTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
                if (startDate < nowDateTemp)
                {
                    startDate = DateTime.Now;
                }
            }
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 1, 0, 0);

            //Generate days names
            List<string> daysNameHtmlList = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                var tempDate = startDate.AddDays(i);
                string dayString = $"<th class='text-center'><div>{tempDate.ToShortDateString()}</div><div>{tempDate.DayOfWeek.ToString()}</div></th>";
                daysNameHtmlList.Add(dayString);
            }
            ViewBag.DaysNames = daysNameHtmlList;


            //Get entries form DB and create List with entries info
            var calendarEntriesDb = _calendarLogic.GetPatientsCalendar(User.Identity.GetUserId(), startDate);
            List<List<CalendarEntryViewModel>> calendarEntriesList = new List<List<CalendarEntryViewModel>>();
            for (int j = 0; j < 14; j++)
            {
                List<CalendarEntryViewModel> tempList = new List<CalendarEntryViewModel>();
                for (int i = 0; i < 7; i++)
                {
                    var tempDate = startDate.AddDays(i);
                    var dateToCompare = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 8 + j, 0, 0);
                    var calEntry = new CalendarEntryViewModel
                    {
                        Time = (8 + j).ToString(),
                        Status = "E",
                        Date = tempDate.ToShortDateString()
                    };

                    foreach (var entryFromDb in calendarEntriesDb)
                    {
                        if (dateToCompare.Ticks == entryFromDb.Date.Ticks)
                        {
                            if (entryFromDb.Free == true)
                            {
                                calEntry.Status = "F";
                            }
                            else
                            {
                                calEntry.Status = "M";
                                calEntry.Name = entryFromDb.Doctor.FullName;
                                calEntry.UserId = entryFromDb.DoctorId;
                                if (entryFromDb.Confirmed == false)
                                {
                                    calEntry.Status = "P";
                                }
                            }
                        }
                    }
                    tempList.Add(calEntry);
                }
                calendarEntriesList.Add(tempList);
            }

            ViewBag.CalendarEntries = calendarEntriesList;
            ViewBag.PatientId = User.Identity.GetUserId();

            var prevDate = startDate.AddDays(-7);
            prevDate = new DateTime(prevDate.Year, prevDate.Month, prevDate.Day, 1, 0, 0);
            var nowDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
            if (prevDate < nowDate)
            {
                prevDate = DateTime.Now;
            }
            var nextDate = startDate.AddDays(7);

            ViewBag.PrevDate = prevDate.ToShortDateString();
            ViewBag.NextDate = nextDate.ToShortDateString();

            return View();
        }
        #endregion

        #region Ajax Actions

        [HttpPost]
        public ActionResult InvertFreeEntry(string date, string time)
        {
            try
            {
                DateTime dateTime = DateTime.Parse(date);
                dateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, int.Parse(time), 0, 0);

                bool isSuccessful = _calendarLogic.InvertFreeEntry(dateTime, User.Identity.GetUserId());
                if (isSuccessful == false)
                {
                    return Json("false");
                }

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<CalendarHub>();
                hubContext.Clients.Group(User.Identity.GetUserId()).invertFreeEntry(dateTime.ToShortDateString(), dateTime.Hour);
            }
            catch (Exception e)
            {
                throw;
                return Json("error");
            }

            return Json("ok");
        }

        [HttpPost]
        public ActionResult InvertEmptyEntry(string date, string time)
        {
            try
            {
                DateTime dateTime = DateTime.Parse(date);
                var entryDate = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, int.Parse(time), 0, 0);
                var entry = new Calendar
                {
                    Confirmed = false,
                    Free = true,
                    DoctorId = User.Identity.GetUserId(),
                    Date = entryDate

                };

                _calendarLogic.AddEntry(entry);
                _calendarLogic.SaveChanges();

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<CalendarHub>();
                hubContext.Clients.Group(User.Identity.GetUserId()).invertEmptyEntry(entry.Date.ToShortDateString(), entry.Date.Hour);
            }
            catch (Exception e)
            {
                throw;
                return Json("error");
            }

            return Json("ok");
        }

        [HttpPost]
        public ActionResult ReserveEntry(string date, string time, string doctorId)
        {
            try
            {
                DateTime dateTime = DateTime.Parse(date);
                var entryDate = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, int.Parse(time), 0, 0);

                bool isSuccessful = _calendarLogic.ReserveEntry(entryDate, doctorId, User.Identity.GetUserId());
                if (isSuccessful == false)
                {
                    return Json("false");
                }

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<CalendarHub>();
                hubContext.Clients.All.reserveEntry(entryDate.ToShortDateString(), entryDate.Hour, doctorId, User.Identity.GetUserId(), _patientLogic.Get(User.Identity.GetUserId()).FullName);
            }
            catch (Exception e)
            {
                throw;
                return Json("error");
            }

            return Json("ok");
        }

        [HttpPost]
        public ActionResult RejectEntryDoctor(string date, string time)
        {
            DateTime dateTime = DateTime.Parse(date);
            dateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, int.Parse(time), 0, 0);

            bool isSuccessful = _calendarLogic.RejectEntryDoctor(dateTime, User.Identity.GetUserId());
            if (isSuccessful == false)
            {
                return Json("false");
            }

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<CalendarHub>();
            hubContext.Clients.All.rejectEntryDoctor(dateTime.ToShortDateString(), dateTime.Hour, User.Identity.GetUserId());

            return Json("ok");
        }

        [HttpPost]
        public ActionResult ConfirmEntryDoctor(string date, string time)
        {
            try
            {
                DateTime dateTime = DateTime.Parse(date);
                dateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, int.Parse(time), 0, 0);

                bool isSuccessful = _calendarLogic.ConfirmEntryDoctor(dateTime, User.Identity.GetUserId());
                if (isSuccessful == false)
                {
                    return Json("false");
                }

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<CalendarHub>();
                hubContext.Clients.All.confirmEntryDoctor(dateTime.ToShortDateString(), dateTime.Hour, User.Identity.GetUserId());
            }
            catch (Exception e)
            {
                throw;
                return Json("error");
            }

            return Json("ok");
        }

        [HttpPost]
        public ActionResult CancelReservedDoctor(string date, string time, string patientId)
        {
            try
            {
                DateTime dateTime = DateTime.Parse(date);
                dateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, int.Parse(time), 0, 0);

                bool isSuccessful = _calendarLogic.CancelReservedDoctor(dateTime, User.Identity.GetUserId());
                if (isSuccessful == false)
                {
                    return Json("false");
                }

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<CalendarHub>();
                hubContext.Clients.All.cancelReservedDoctor(dateTime.ToShortDateString(), dateTime.Hour, User.Identity.GetUserId(), patientId);
            }
            catch (Exception e)
            {
                throw;
                return Json("error");
            }

            return Json("ok");
        }

        [HttpPost]
        public ActionResult CancelReservedPatient(string date, string time, string doctorId)
        {
            try
            {
                DateTime dateTime = DateTime.Parse(date);
                dateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, int.Parse(time), 0, 0);

                var cal = _calendarLogic.GetEntry(dateTime, doctorId);
                if (cal == null || cal.PatientId != User.Identity.GetUserId())
                {
                    return Json("false");
                }

                bool isSuccessful = _calendarLogic.CancelReservedDoctor(dateTime, doctorId);
                if (isSuccessful == false)
                {
                    return Json("false");
                }

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<CalendarHub>();
                hubContext.Clients.All.cancelReservedPatient(dateTime.ToShortDateString(), dateTime.Hour, doctorId);
            }
            catch (Exception e)
            {
                throw;
                return Json("error");
            }

            return Json("ok");
        }

        #endregion

    }
}