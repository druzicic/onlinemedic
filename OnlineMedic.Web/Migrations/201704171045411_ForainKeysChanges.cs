namespace OnlineMedic.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForainKeysChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Flags", "Doctor_UserId", "dbo.Doctors");
            DropForeignKey("dbo.Ratings", "Doctor_UserId", "dbo.Doctors");
            DropForeignKey("dbo.Records", "Doctor_UserId", "dbo.Doctors");
            DropForeignKey("dbo.Services", "Doctor_UserId", "dbo.Doctors");
            DropForeignKey("dbo.Flags", "Patient_UserId", "dbo.Patients");
            DropForeignKey("dbo.Ratings", "Patient_UserId", "dbo.Patients");
            DropForeignKey("dbo.Records", "Patient_UserId", "dbo.Patients");
            DropIndex("dbo.Flags", new[] { "Doctor_UserId" });
            DropIndex("dbo.Flags", new[] { "Patient_UserId" });
            DropIndex("dbo.Ratings", new[] { "Doctor_UserId" });
            DropIndex("dbo.Ratings", new[] { "Patient_UserId" });
            DropIndex("dbo.Records", new[] { "Doctor_UserId" });
            DropIndex("dbo.Records", new[] { "Patient_UserId" });
            DropIndex("dbo.Services", new[] { "Doctor_UserId" });
            DropColumn("dbo.Flags", "DoctorId");
            DropColumn("dbo.Flags", "PatientId");
            DropColumn("dbo.Ratings", "DoctorId");
            DropColumn("dbo.Ratings", "PatientId");
            DropColumn("dbo.Records", "DoctorId");
            DropColumn("dbo.Records", "PatientId");
            DropColumn("dbo.Services", "DoctorId");
            RenameColumn(table: "dbo.Doctors", name: "Category_Id", newName: "CategoryId");
            RenameColumn(table: "dbo.Flags", name: "Doctor_UserId", newName: "DoctorId");
            RenameColumn(table: "dbo.Ratings", name: "Doctor_UserId", newName: "DoctorId");
            RenameColumn(table: "dbo.Records", name: "Doctor_UserId", newName: "DoctorId");
            RenameColumn(table: "dbo.Services", name: "Doctor_UserId", newName: "DoctorId");
            RenameColumn(table: "dbo.Flags", name: "Patient_UserId", newName: "PatientId");
            RenameColumn(table: "dbo.Ratings", name: "Patient_UserId", newName: "PatientId");
            RenameColumn(table: "dbo.Records", name: "Patient_UserId", newName: "PatientId");
            RenameIndex(table: "dbo.Doctors", name: "IX_Category_Id", newName: "IX_CategoryId");
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Flags", "DoctorId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Flags", "PatientId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Flags", "DoctorId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Flags", "PatientId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Ratings", "DoctorId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Ratings", "PatientId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Ratings", "DoctorId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Ratings", "PatientId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Records", "DoctorId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Records", "PatientId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Records", "DoctorId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Records", "PatientId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Services", "DoctorId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Services", "DoctorId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Flags", "DoctorId");
            CreateIndex("dbo.Flags", "PatientId");
            CreateIndex("dbo.Ratings", "DoctorId");
            CreateIndex("dbo.Ratings", "PatientId");
            CreateIndex("dbo.Records", "DoctorId");
            CreateIndex("dbo.Records", "PatientId");
            CreateIndex("dbo.Services", "DoctorId");
            AddForeignKey("dbo.Flags", "DoctorId", "dbo.Doctors", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Ratings", "DoctorId", "dbo.Doctors", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Records", "DoctorId", "dbo.Doctors", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Services", "DoctorId", "dbo.Doctors", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Flags", "PatientId", "dbo.Patients", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Ratings", "PatientId", "dbo.Patients", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Records", "PatientId", "dbo.Patients", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Records", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Ratings", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Flags", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Services", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.Records", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.Ratings", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.Flags", "DoctorId", "dbo.Doctors");
            DropIndex("dbo.Services", new[] { "DoctorId" });
            DropIndex("dbo.Records", new[] { "PatientId" });
            DropIndex("dbo.Records", new[] { "DoctorId" });
            DropIndex("dbo.Ratings", new[] { "PatientId" });
            DropIndex("dbo.Ratings", new[] { "DoctorId" });
            DropIndex("dbo.Flags", new[] { "PatientId" });
            DropIndex("dbo.Flags", new[] { "DoctorId" });
            AlterColumn("dbo.Services", "DoctorId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Services", "DoctorId", c => c.String(nullable: false));
            AlterColumn("dbo.Records", "PatientId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Records", "DoctorId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Records", "PatientId", c => c.String(nullable: false));
            AlterColumn("dbo.Records", "DoctorId", c => c.String(nullable: false));
            AlterColumn("dbo.Ratings", "PatientId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Ratings", "DoctorId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Ratings", "PatientId", c => c.String(nullable: false));
            AlterColumn("dbo.Ratings", "DoctorId", c => c.String(nullable: false));
            AlterColumn("dbo.Flags", "PatientId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Flags", "DoctorId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Flags", "PatientId", c => c.String(nullable: false));
            AlterColumn("dbo.Flags", "DoctorId", c => c.String(nullable: false));
            AlterColumn("dbo.Categories", "Name", c => c.String());
            RenameIndex(table: "dbo.Doctors", name: "IX_CategoryId", newName: "IX_Category_Id");
            RenameColumn(table: "dbo.Records", name: "PatientId", newName: "Patient_UserId");
            RenameColumn(table: "dbo.Ratings", name: "PatientId", newName: "Patient_UserId");
            RenameColumn(table: "dbo.Flags", name: "PatientId", newName: "Patient_UserId");
            RenameColumn(table: "dbo.Services", name: "DoctorId", newName: "Doctor_UserId");
            RenameColumn(table: "dbo.Records", name: "DoctorId", newName: "Doctor_UserId");
            RenameColumn(table: "dbo.Ratings", name: "DoctorId", newName: "Doctor_UserId");
            RenameColumn(table: "dbo.Flags", name: "DoctorId", newName: "Doctor_UserId");
            RenameColumn(table: "dbo.Doctors", name: "CategoryId", newName: "Category_Id");
            AddColumn("dbo.Services", "DoctorId", c => c.String(nullable: false));
            AddColumn("dbo.Records", "PatientId", c => c.String(nullable: false));
            AddColumn("dbo.Records", "DoctorId", c => c.String(nullable: false));
            AddColumn("dbo.Ratings", "PatientId", c => c.String(nullable: false));
            AddColumn("dbo.Ratings", "DoctorId", c => c.String(nullable: false));
            AddColumn("dbo.Flags", "PatientId", c => c.String(nullable: false));
            AddColumn("dbo.Flags", "DoctorId", c => c.String(nullable: false));
            CreateIndex("dbo.Services", "Doctor_UserId");
            CreateIndex("dbo.Records", "Patient_UserId");
            CreateIndex("dbo.Records", "Doctor_UserId");
            CreateIndex("dbo.Ratings", "Patient_UserId");
            CreateIndex("dbo.Ratings", "Doctor_UserId");
            CreateIndex("dbo.Flags", "Patient_UserId");
            CreateIndex("dbo.Flags", "Doctor_UserId");
            AddForeignKey("dbo.Records", "Patient_UserId", "dbo.Patients", "UserId");
            AddForeignKey("dbo.Ratings", "Patient_UserId", "dbo.Patients", "UserId");
            AddForeignKey("dbo.Flags", "Patient_UserId", "dbo.Patients", "UserId");
            AddForeignKey("dbo.Services", "Doctor_UserId", "dbo.Doctors", "UserId");
            AddForeignKey("dbo.Records", "Doctor_UserId", "dbo.Doctors", "UserId");
            AddForeignKey("dbo.Ratings", "Doctor_UserId", "dbo.Doctors", "UserId");
            AddForeignKey("dbo.Flags", "Doctor_UserId", "dbo.Doctors", "UserId");
        }
    }
}
