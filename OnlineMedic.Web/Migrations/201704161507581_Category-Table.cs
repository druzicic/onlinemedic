namespace OnlineMedic.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Doctors", "Category_Id", c => c.Int());
            CreateIndex("dbo.Doctors", "Category_Id");
            AddForeignKey("dbo.Doctors", "Category_Id", "dbo.Categories", "Id");
            DropColumn("dbo.Records", "ServiceName");
            DropColumn("dbo.Records", "ServicePrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Records", "ServicePrice", c => c.Single(nullable: false));
            AddColumn("dbo.Records", "ServiceName", c => c.String());
            DropForeignKey("dbo.Doctors", "Category_Id", "dbo.Categories");
            DropIndex("dbo.Doctors", new[] { "Category_Id" });
            DropColumn("dbo.Doctors", "Category_Id");
            DropTable("dbo.Categories");
        }
    }
}
