namespace OnlineMedic.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MedicalExamination_Deleted : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MedicalExaminations", "RecordId", "dbo.Records");
            DropIndex("dbo.MedicalExaminations", new[] { "RecordId" });
            AddColumn("dbo.Records", "DoctorId", c => c.String());
            AddColumn("dbo.Records", "PatientId", c => c.String());
            AddColumn("dbo.Records", "RecordId", c => c.Int(nullable: false));
            AddColumn("dbo.Records", "ServiceName", c => c.String());
            AddColumn("dbo.Records", "ServicePrice", c => c.Single(nullable: false));
            AddColumn("dbo.Records", "Date", c => c.DateTime(nullable: false));
            DropTable("dbo.MedicalExaminations");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MedicalExaminations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorId = c.String(),
                        PatientId = c.String(),
                        RecordId = c.Int(nullable: false),
                        ServiceName = c.String(),
                        ServicePrice = c.Single(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Records", "Date");
            DropColumn("dbo.Records", "ServicePrice");
            DropColumn("dbo.Records", "ServiceName");
            DropColumn("dbo.Records", "RecordId");
            DropColumn("dbo.Records", "PatientId");
            DropColumn("dbo.Records", "DoctorId");
            CreateIndex("dbo.MedicalExaminations", "RecordId");
            AddForeignKey("dbo.MedicalExaminations", "RecordId", "dbo.Records", "Id", cascadeDelete: true);
        }
    }
}
