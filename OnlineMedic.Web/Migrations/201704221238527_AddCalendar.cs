namespace OnlineMedic.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCalendar : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Calendars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorId = c.String(nullable: false, maxLength: 128),
                        PatientId = c.String(maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Confirmed = c.Boolean(nullable: false),
                        Free = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Doctors", t => t.DoctorId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.PatientId)
                .Index(t => t.DoctorId)
                .Index(t => t.PatientId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Calendars", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Calendars", "DoctorId", "dbo.Doctors");
            DropIndex("dbo.Calendars", new[] { "PatientId" });
            DropIndex("dbo.Calendars", new[] { "DoctorId" });
            DropTable("dbo.Calendars");
        }
    }
}
