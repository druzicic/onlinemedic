namespace OnlineMedic.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecordField_Deleted : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Records", "RecordId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Records", "RecordId", c => c.Int(nullable: false));
        }
    }
}
