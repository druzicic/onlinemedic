namespace OnlineMedic.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RatingAndFlagAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Flags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorId = c.String(),
                        PatientId = c.String(),
                        Comment = c.String(),
                        Doctor_UserId = c.String(maxLength: 128),
                        Patient_UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Doctors", t => t.Doctor_UserId)
                .ForeignKey("dbo.Patients", t => t.Patient_UserId)
                .Index(t => t.Doctor_UserId)
                .Index(t => t.Patient_UserId);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorId = c.String(),
                        PatientId = c.String(),
                        Score = c.Int(nullable: false),
                        Comment = c.String(),
                        Doctor_UserId = c.String(maxLength: 128),
                        Patient_UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Doctors", t => t.Doctor_UserId)
                .ForeignKey("dbo.Patients", t => t.Patient_UserId)
                .Index(t => t.Doctor_UserId)
                .Index(t => t.Patient_UserId);
            
            AddColumn("dbo.Records", "Doctor_UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.Records", "Patient_UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Records", "Doctor_UserId");
            CreateIndex("dbo.Records", "Patient_UserId");
            AddForeignKey("dbo.Records", "Doctor_UserId", "dbo.Doctors", "UserId");
            AddForeignKey("dbo.Records", "Patient_UserId", "dbo.Patients", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Records", "Patient_UserId", "dbo.Patients");
            DropForeignKey("dbo.Ratings", "Patient_UserId", "dbo.Patients");
            DropForeignKey("dbo.Flags", "Patient_UserId", "dbo.Patients");
            DropForeignKey("dbo.Records", "Doctor_UserId", "dbo.Doctors");
            DropForeignKey("dbo.Ratings", "Doctor_UserId", "dbo.Doctors");
            DropForeignKey("dbo.Flags", "Doctor_UserId", "dbo.Doctors");
            DropIndex("dbo.Records", new[] { "Patient_UserId" });
            DropIndex("dbo.Records", new[] { "Doctor_UserId" });
            DropIndex("dbo.Ratings", new[] { "Patient_UserId" });
            DropIndex("dbo.Ratings", new[] { "Doctor_UserId" });
            DropIndex("dbo.Flags", new[] { "Patient_UserId" });
            DropIndex("dbo.Flags", new[] { "Doctor_UserId" });
            DropColumn("dbo.Records", "Patient_UserId");
            DropColumn("dbo.Records", "Doctor_UserId");
            DropTable("dbo.Ratings");
            DropTable("dbo.Flags");
        }
    }
}
