// <auto-generated />
namespace OnlineMedic.Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class MedicalExamination_Deleted : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MedicalExamination_Deleted));
        
        string IMigrationMetadata.Id
        {
            get { return "201702190927580_MedicalExamination_Deleted"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
