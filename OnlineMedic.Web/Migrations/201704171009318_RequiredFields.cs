namespace OnlineMedic.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredFields : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Doctors", "Category_Id", "dbo.Categories");
            DropIndex("dbo.Doctors", new[] { "Category_Id" });
            AddColumn("dbo.Doctors", "Latitude", c => c.Double(nullable: false));
            AddColumn("dbo.Doctors", "Longitude", c => c.Double(nullable: false));
            AlterColumn("dbo.Doctors", "FullName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Doctors", "Biography", c => c.String(nullable: false));
            AlterColumn("dbo.Doctors", "Category_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Flags", "DoctorId", c => c.String(nullable: false));
            AlterColumn("dbo.Flags", "PatientId", c => c.String(nullable: false));
            AlterColumn("dbo.Flags", "Comment", c => c.String(nullable: false));
            AlterColumn("dbo.Ratings", "DoctorId", c => c.String(nullable: false));
            AlterColumn("dbo.Ratings", "PatientId", c => c.String(nullable: false));
            AlterColumn("dbo.Records", "DoctorId", c => c.String(nullable: false));
            AlterColumn("dbo.Records", "PatientId", c => c.String(nullable: false));
            AlterColumn("dbo.Records", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Records", "Text", c => c.String(nullable: false));
            AlterColumn("dbo.RecordFiles", "FileLocation", c => c.String(nullable: false));
            AlterColumn("dbo.Services", "DoctorId", c => c.String(nullable: false));
            AlterColumn("dbo.Services", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Patients", "FullName", c => c.String(nullable: false, maxLength: 50));
            CreateIndex("dbo.Doctors", "Category_Id");
            AddForeignKey("dbo.Doctors", "Category_Id", "dbo.Categories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Doctors", "Category_Id", "dbo.Categories");
            DropIndex("dbo.Doctors", new[] { "Category_Id" });
            AlterColumn("dbo.Patients", "FullName", c => c.String());
            AlterColumn("dbo.Services", "Name", c => c.String());
            AlterColumn("dbo.Services", "DoctorId", c => c.String());
            AlterColumn("dbo.RecordFiles", "FileLocation", c => c.String());
            AlterColumn("dbo.Records", "Text", c => c.String());
            AlterColumn("dbo.Records", "Title", c => c.String());
            AlterColumn("dbo.Records", "PatientId", c => c.String());
            AlterColumn("dbo.Records", "DoctorId", c => c.String());
            AlterColumn("dbo.Ratings", "PatientId", c => c.String());
            AlterColumn("dbo.Ratings", "DoctorId", c => c.String());
            AlterColumn("dbo.Flags", "Comment", c => c.String());
            AlterColumn("dbo.Flags", "PatientId", c => c.String());
            AlterColumn("dbo.Flags", "DoctorId", c => c.String());
            AlterColumn("dbo.Doctors", "Category_Id", c => c.Int());
            AlterColumn("dbo.Doctors", "Biography", c => c.String());
            AlterColumn("dbo.Doctors", "FullName", c => c.String());
            DropColumn("dbo.Doctors", "Longitude");
            DropColumn("dbo.Doctors", "Latitude");
            CreateIndex("dbo.Doctors", "Category_Id");
            AddForeignKey("dbo.Doctors", "Category_Id", "dbo.Categories", "Id");
        }
    }
}
