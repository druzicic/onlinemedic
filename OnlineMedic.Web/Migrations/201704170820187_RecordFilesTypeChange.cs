namespace OnlineMedic.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecordFilesTypeChange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RecordFiles", "Record_Id", "dbo.Records");
            DropIndex("dbo.RecordFiles", new[] { "Record_Id" });
            DropColumn("dbo.RecordFiles", "RecordId");
            RenameColumn(table: "dbo.RecordFiles", name: "Record_Id", newName: "RecordId");
            AlterColumn("dbo.RecordFiles", "RecordId", c => c.Int(nullable: false));
            AlterColumn("dbo.RecordFiles", "RecordId", c => c.Int(nullable: false));
            CreateIndex("dbo.RecordFiles", "RecordId");
            AddForeignKey("dbo.RecordFiles", "RecordId", "dbo.Records", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecordFiles", "RecordId", "dbo.Records");
            DropIndex("dbo.RecordFiles", new[] { "RecordId" });
            AlterColumn("dbo.RecordFiles", "RecordId", c => c.Int());
            AlterColumn("dbo.RecordFiles", "RecordId", c => c.String());
            RenameColumn(table: "dbo.RecordFiles", name: "RecordId", newName: "Record_Id");
            AddColumn("dbo.RecordFiles", "RecordId", c => c.String());
            CreateIndex("dbo.RecordFiles", "Record_Id");
            AddForeignKey("dbo.RecordFiles", "Record_Id", "dbo.Records", "Id");
        }
    }
}
