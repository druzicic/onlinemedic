﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.ViewModels
{
    public class DoctorViewModel
    {
        public string UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string FullName { get; set; }

        [Required]
        public float ServicePrice { get; set; }

        [Required]
        public string Biography { get; set; }

        [Required]
        public double Latitude { get; set; }

        [Required]
        public double Longitude { get; set; }

        [Required]
        public int CategoryId { get; set; }
    }
}