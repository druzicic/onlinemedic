﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.ViewModels
{
    public class CalendarEntryViewModel
    {
        public string Time { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public string Date { get; set; }
    }
}