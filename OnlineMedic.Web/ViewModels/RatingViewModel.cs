﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.ViewModels
{
    public class RatingViewModel
    {
        public int Id { get; set; }

        
        public string DoctorId { get; set; }

        
        public string PatientId { get; set; }

        [Required]
        public int Score { get; set; }

        public string Comment { get; set; }
    }
}