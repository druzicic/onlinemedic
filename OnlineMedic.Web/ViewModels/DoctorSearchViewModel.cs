﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.ViewModels
{
    public class DoctorSearchViewModel
    {
        public float? MinPrice { get; set; }
        public float? MaxPrice { get; set; }
        public double? MaxDistance { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
    }
}