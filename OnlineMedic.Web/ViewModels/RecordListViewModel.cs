﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.ViewModels
{
    public class RecordListViewModel
    {
        public int Id { get; set; }
        public string DoctorId { get; set; }
        public string DoctorName { get; set; }
        public string PatientId { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
    }
}