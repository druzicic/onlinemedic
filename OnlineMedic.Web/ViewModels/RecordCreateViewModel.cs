﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.ViewModels
{
    public class RecordCreateViewModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Text { get; set; }
    }
}