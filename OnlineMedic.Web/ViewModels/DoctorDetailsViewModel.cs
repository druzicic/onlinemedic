﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.ViewModels
{
    public class DoctorDetailsViewModel
    {
        public string UserId { get; set; }
        
        public string FullName { get; set; }
        
        public float ServicePrice { get; set; }
        
        public string Biography { get; set; }
        
        public double Latitude { get; set; }
        
        public double Longitude { get; set; }
        
        public string Category { get; set; }

        public double Ratings { get; set; }

        public List<string> Comments { get; set; }
    }
}