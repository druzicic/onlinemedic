﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.ViewModels
{
    public class DoctorListViewModel
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public float ServicePrice { get; set; }
        public double Ratings { get; set; }
        public string Category { get; set; }
    }
}