﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace OnlineMedic.Web.Filters
{
    public class RolesAuthorizationFilter : ActionFilterAttribute, IActionFilter
    {
        public string Roles { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Thread.CurrentPrincipal.IsInRole(Roles) == false)
            {
                filterContext.Result = new ViewResult() { ViewName = "Error_403" };
            }

            base.OnActionExecuting(filterContext);
        }
    }
}