﻿$(document).ready(function () {
    var calendar = $.connection.calendarHub;

    calendar.client.invertEmptyEntry = function (date, time) {
        var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

        var url = window.location.href;
        var array = url.split('/');
        var lastsegment = array[array.length - 1];

        if (lastsegment.toLowerCase().startsWith("mycalendardoctor")) {

        }
        else if (lastsegment.toLowerCase().startsWith("mycalendarpatient")) {

        }
        else {
            $(selectedEntry).html("Free");
            $(selectedEntry).addClass("alert");
            $(selectedEntry).addClass("alert-success");
            $(selectedEntry).addClass("entry-record");
            $(selectedEntry).addClass("cursor-pointer");
            $(selectedEntry).attr("data-status", "F");
        }

        
    };

    calendar.client.invertFreeEntry = function (date, time) {
        var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

        var url = window.location.href;
        var array = url.split('/');
        var lastsegment = array[array.length - 1];

        if (lastsegment.toLowerCase().startsWith("mycalendardoctor")) {

        }
        else if (lastsegment.toLowerCase().startsWith("mycalendarpatient")) {

        }
        else {
            $(selectedEntry).html("");
            $(selectedEntry).removeClass("alert");
            $(selectedEntry).removeClass("alert-success");
            $(selectedEntry).removeClass("entry-record");
            $(selectedEntry).removeClass("cursor-pointer");
            $(selectedEntry).attr("data-status", "R");
        }
    };

    calendar.client.rejectEntryDoctor = function (date, time, doctorId) {        
        var url = window.location.href;
        var array = url.split('/');
        var lastsegment = array[array.length - 1];

        if (lastsegment.toLowerCase().startsWith("mycalendardoctor")) {

        }
        else if (lastsegment.toLowerCase().startsWith("mycalendarpatient")) {
  
            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");
            
            if ($(selectedEntry).attr("data-doctor-id") != doctorId) {
                return;
            }

            $(selectedEntry).html("");
            $(selectedEntry).removeClass("alert");
            $(selectedEntry).removeClass("alert-info");
            $(selectedEntry).removeClass("entry-record");
            $(selectedEntry).removeClass("cursor-pointer");
            $(selectedEntry).attr("data-status", "E");
        }
        else {
            var array2 = lastsegment.split('?');
            if (array2[0] != doctorId) {
                return;
            }

            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

            $(selectedEntry).html("Free");
            $(selectedEntry).removeClass("alert-info");
            $(selectedEntry).addClass("alert-success");
            $(selectedEntry).attr("data-status", "F");
        }
    };

    calendar.client.confirmEntryDoctor = function (date, time, doctorId) {
        var url = window.location.href;
        var array = url.split('/');
        var lastsegment = array[array.length - 1];

        if (lastsegment.toLowerCase().startsWith("mycalendardoctor")) {

        }
        else if (lastsegment.toLowerCase().startsWith("mycalendarpatient")) {

            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

            if ($(selectedEntry).attr("data-doctor-id") != doctorId) {
                return;
            }

            $(selectedEntry).removeClass("alert-info");
            $(selectedEntry).addClass("alert-danger");
            $(selectedEntry).attr("data-status", "M");
        }
        else {
            var array2 = lastsegment.split('?');
            if (array2[0] != doctorId) {
                return;
            }

            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

            //$(selectedEntry).html("Free");
            $(selectedEntry).removeClass("alert-info");
            $(selectedEntry).addClass("alert-danger");
            $(selectedEntry).attr("data-status", "M");
        }
    };

    calendar.client.reserveEntry = function (date, time, doctorId, patientId, patientName) {
        var url = window.location.href;
        var array = url.split('/');
        var lastsegment = array[array.length - 1];

        if (lastsegment.toLowerCase().startsWith("mycalendardoctor")) {
            if (_doctorId != doctorId) {
                return;
            }

            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

            $(selectedEntry).html(patientName);
            $(selectedEntry).removeClass("alert-success");
            $(selectedEntry).addClass("alert-info");
            $(selectedEntry).attr("data-status", "P");

            $(selectedEntry).attr("data-patient-id", patientId);
        }
        else if (lastsegment.toLowerCase().startsWith("mycalendarpatient")) {
            
        }
        else {
            var array2 = lastsegment.split('?');
            if (array2[0] != doctorId) {
                return;
            }
            if (_patientId == patientId) {
                return;
            }

            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

            //$(selectedEntry).html("Free");
            $(selectedEntry).html("");
            $(selectedEntry).removeClass("alert");
            $(selectedEntry).removeClass("alert-success");
            $(selectedEntry).removeClass("entry-record");
            $(selectedEntry).removeClass("cursor-pointer");
            $(selectedEntry).attr("data-status", "R");
        }
    };

    calendar.client.cancelReservedDoctor = function (date, time, doctorId, patientId) {
        var url = window.location.href;
        var array = url.split('/');
        var lastsegment = array[array.length - 1];

        if (lastsegment.toLowerCase().startsWith("mycalendardoctor")) {
            
        }
        else if (lastsegment.toLowerCase().startsWith("mycalendarpatient")) {
            if (_patientId != patientId) {
                return;
            }

            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

            if ($(selectedEntry).attr("data-doctor-id") != doctorId) {
                return;
            }

            //$(selectedEntry).html("Free");
            $(selectedEntry).html("");
            $(selectedEntry).removeClass("alert");
            $(selectedEntry).removeClass("alert-danger");
            $(selectedEntry).removeClass("entry-record");
            $(selectedEntry).removeClass("cursor-pointer");
            $(selectedEntry).attr("data-status", "E");
        }
        else {
            var array2 = lastsegment.split('?');
            if (array2[0] != doctorId) {
                return;
            }

            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

            //$(selectedEntry).html("Free");
            $(selectedEntry).removeClass("alert-danger");

            $(selectedEntry).html("Free");
            $(selectedEntry).addClass("alert");
            $(selectedEntry).addClass("alert-success");
            $(selectedEntry).addClass("entry-record");
            $(selectedEntry).addClass("cursor-pointer");
            $(selectedEntry).attr("data-status", "F");
        }
    };

    calendar.client.cancelReservedPatient = function (date, time, doctorId) {
        var url = window.location.href;
        var array = url.split('/');
        var lastsegment = array[array.length - 1];

        if (lastsegment.toLowerCase().startsWith("mycalendardoctor")) {
            if (_doctorId != doctorId) {
                return;
            }

            var selectedEntry = $("div[data-date='" + date + "'][data-time='" + time + "']");

            $(selectedEntry).html("Free");
            $(selectedEntry).removeClass("alert-info");
            $(selectedEntry).removeClass("alert-danger");
            $(selectedEntry).addClass("alert-success");
            $(selectedEntry).attr("data-status", "F");
        }
        else if (lastsegment.toLowerCase().startsWith("mycalendarpatient")) {

        }
        else {

        }
    };

    function initialize() {
        var url = window.location.href;
        var array = url.split('/');
        var lastsegment = array[array.length - 1];

        if (lastsegment.toLowerCase().startsWith("mycalendardoctor")) {

        }
        else if (lastsegment.toLowerCase().startsWith("mycalendarpatient")) {

        }
        else {
            var array2 = lastsegment.split('?');
            calendar.server.joinDoctorCalendar(array2[0]);
        }
    }

    setTimeout(initialize, 1000);

});