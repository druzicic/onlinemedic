﻿using Autofac;
using Autofac.Integration.Mvc;
using OnlineMedic.Data.Infrastructure;
using OnlineMedic.Data.Repositories;
using OnlineMedic.Web.BusinessLogic;
using OnlineMedic.Web.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace OnlineMedic.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SetAutofacContainer();
            MappingConfig.RegisterMaps();
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<UserLogic>().As<IUserLogic>().InstancePerRequest();
            builder.RegisterType<DoctorLogic>().As<IDoctorLogic>().InstancePerRequest();
            builder.RegisterType<PatientLogic>().As<IPatientLogic>().InstancePerRequest();
            builder.RegisterType<MedicalExaminationLogic>().As<IMedicalExaminationLogic>().InstancePerRequest();
            builder.RegisterType<CalendarLogic>().As<ICalendarLogic>().InstancePerRequest();
            builder.RegisterType<NotificationLogic>().As<INotificationLogic>().InstancePerRequest();

            builder.RegisterType<DoctorRepository>().As<IDoctorRepository>().InstancePerRequest();
            builder.RegisterType<PatientRepository>().As<IPatientRepository>().InstancePerRequest();
            builder.RegisterType<RecordRepository>().As<IRecordRepository>().InstancePerRequest();
            builder.RegisterType<CalendarRepository>().As<ICalendarRepository>().InstancePerRequest();
            builder.RegisterType<NotificationRepository>().As<INotificationRepository>().InstancePerRequest();

            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
