﻿using AutoMapper;
using OnlineMedic.Data.DataTransport;
using OnlineMedic.Data.Models;
using OnlineMedic.Web.Models;
using OnlineMedic.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.Mappings
{
    public static class MappingConfig
    {
        public static void RegisterMaps()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<DoctorShort, DoctorListViewModel>();
                config.CreateMap<Doctor, DoctorViewModel>();
                config.CreateMap<Patient, PatientListViewModel>();
                config.CreateMap<RecordShort, RecordListViewModel>();
                config.CreateMap<Record, RecordViewModel>();
            });
        }
    }
}