﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OnlineMedic.Web.Startup))]
namespace OnlineMedic.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureSignalR(app);
        }
    }
}
