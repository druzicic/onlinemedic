﻿using OnlineMedic.Data.DataTransport;
using OnlineMedic.Data.Models;
using OnlineMedic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.BusinessLogic
{
    public class DoctorLogic : IDoctorLogic
    {
        private readonly IDoctorRepository _doctorRepository;
        public DoctorLogic(IDoctorRepository dr)
        {
            _doctorRepository = dr;
        }

        public void Add(Doctor doctor)
        {
            _doctorRepository.Add(doctor);
        }

        public void AddRating(Rating rating)
        {
            _doctorRepository.AddRating(rating);
        }

        public Doctor GetById(string id)
        {
            return _doctorRepository.GetById(id);
        }

        public IEnumerable<Category> GetCategories()
        {
            return _doctorRepository.GetCategories();
        }

        public IEnumerable<Doctor> GetDoctorsList()
        {
            return _doctorRepository.GetAll();
        }

        public IEnumerable<DoctorShort> GetDoctorsShort(int? categoryId = null, float? minPrice = null, float? maxPrice = null, double? maxDistance = null, double latitude = 0, double longitude = 0)
        {
            return _doctorRepository.GetDoctorsShort(categoryId, minPrice, maxPrice, maxDistance, latitude, longitude);
        }

        public void SaveChanges()
        {
            _doctorRepository.SaveChanges();
        }

        public void Update(Doctor doctor)
        {
            _doctorRepository.Update(doctor);
        }
    }

    public interface IDoctorLogic
    {
        Doctor GetById(string id);
        void Add(Doctor doctor);
        IEnumerable<Doctor> GetDoctorsList();
        IEnumerable<DoctorShort> GetDoctorsShort(int? categoryId = null, float? minPrice = null, float? maxPrice = null, double? maxDistance = null, double latitude = 0, double longitude = 0);
        void Update(Doctor doctor);
        IEnumerable<Category> GetCategories();
        void AddRating(Rating rating);
        void SaveChanges();
    }
}