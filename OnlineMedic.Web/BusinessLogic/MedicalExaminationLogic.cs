﻿using OnlineMedic.Data.DataTransport;
using OnlineMedic.Data.Models;
using OnlineMedic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.BusinessLogic
{
    public class MedicalExaminationLogic : IMedicalExaminationLogic
    {
        private readonly IRecordRepository _recordRepository;
        public MedicalExaminationLogic(IRecordRepository mr)
        {
            _recordRepository = mr;
        }

        public void AddRecord(Record record)
        {
            _recordRepository.Add(record);
        }

        public void AddRecordFile(RecordFile file)
        {
            _recordRepository.AddRecordFile(file);
        }

        public IEnumerable<RecordShort> GetAllRecordsFromPatientShort(string id)
        {
            return _recordRepository.GetPatientRecordsShortInfo(id);
        }

        public Record GetRecord(int id)
        {
            return _recordRepository.GetFullRecord(id);
        }

        public void SaveChanges()
        {
            _recordRepository.SaveChanges();
        }
    }

    public interface IMedicalExaminationLogic
    {
        IEnumerable<RecordShort> GetAllRecordsFromPatientShort(string id);
        void AddRecord(Record record);

        Record GetRecord(int id);

        void SaveChanges();
        void AddRecordFile(RecordFile file);
    }
}