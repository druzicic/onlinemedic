﻿using OnlineMedic.Data.Models;
using OnlineMedic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.BusinessLogic
{
    public class CalendarLogic : ICalendarLogic
    {
        private readonly ICalendarRepository _calendarRepository;
        public CalendarLogic(ICalendarRepository cr)
        {
            _calendarRepository = cr;
        }

        public void AddEntry(Calendar entry)
        {
            _calendarRepository.Add(entry);
        }

        public bool CancelReservedDoctor(DateTime dateTime, string doctorId)
        {
            return _calendarRepository.CancelReservedDoctor(dateTime, doctorId);
        }

        public bool ConfirmEntryDoctor(DateTime dateTime, string doctorId)
        {
            return _calendarRepository.ConfirmEntryDoctor(dateTime, doctorId);
        }

        public bool InvertFreeEntry(DateTime dateTime, string doctorId)
        {
            return _calendarRepository.InvertFreeEntry(dateTime, doctorId);
        }

        public IEnumerable<Calendar> GetDoctorsCalendar(string doctorId, DateTime date)
        {
            return _calendarRepository.GetDoctorsCalendar(doctorId, date);
        }

        public bool RejectEntryDoctor(DateTime dateTime, string doctorId)
        {
            return _calendarRepository.RejectEntryDoctor(dateTime, doctorId);
        }

        public bool ReserveEntry(DateTime dateTime, string doctorId, string patientId)
        {
            return _calendarRepository.ReserveEntry(dateTime, doctorId, patientId);
        }

        public void SaveChanges()
        {
            _calendarRepository.SaveChanges();
        }

        public Calendar GetEntry(DateTime dateTime, string doctorId)
        {
            return _calendarRepository.GetEntry(dateTime, doctorId);
        }

        public IEnumerable<Calendar> GetPatientsCalendar(string patientId, DateTime date)
        {
            return _calendarRepository.GetPatientsCalendar(patientId, date);
        }
    }

    public interface ICalendarLogic
    {
        IEnumerable<Calendar> GetDoctorsCalendar(string doctorId, DateTime date);
        bool InvertFreeEntry(DateTime dateTime, string doctorId);
        void AddEntry(Calendar entry);
        bool ReserveEntry(DateTime dateTime, string doctorId, string patientId);
        bool RejectEntryDoctor(DateTime dateTime, string doctorId);
        bool ConfirmEntryDoctor(DateTime dateTime, string doctorId);
        bool CancelReservedDoctor(DateTime dateTime, string doctorId);
        Calendar GetEntry(DateTime dateTime, string doctorId);
        IEnumerable<Calendar> GetPatientsCalendar(string patientId, DateTime date);
        void SaveChanges();
    }
}