﻿using OnlineMedic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.BusinessLogic
{
    public class NotificationLogic : INotificationLogic
    {
        private readonly INotificationRepository _notificationRepository;
        public NotificationLogic(INotificationRepository nr)
        {
            _notificationRepository = nr;
        }


        public void SaveChanges()
        {
            _notificationRepository.SaveChanges();
        }
    }

    public interface INotificationLogic
    {
        void SaveChanges();
    }
}