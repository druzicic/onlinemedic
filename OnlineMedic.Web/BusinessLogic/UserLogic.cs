﻿using OnlineMedic.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.BusinessLogic
{
    public class UserLogic : IUserLogic
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public IEnumerable<string> GetRoles()
        {
            return db.Roles.Where(u => !u.Name.Contains("Admin")).Select(r => r.Name).ToList();
        }
    }

    public interface IUserLogic
    {
        IEnumerable<string> GetRoles();
    }
}