﻿using OnlineMedic.Data.Models;
using OnlineMedic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMedic.Web.BusinessLogic
{
    public class PatientLogic : IPatientLogic
    {
        private readonly IPatientRepository _patientRepository;
        public PatientLogic(IPatientRepository dr)
        {
            _patientRepository = dr;
        }

        public void Add(Patient patient)
        {
            _patientRepository.Add(patient);
        }

        public Patient Get(string id)
        {
            return _patientRepository.GetById(id);
        }

        public IEnumerable<Patient> GetAll()
        {
            return _patientRepository.GetAll();
        }

        public void SaveChanges()
        {
            _patientRepository.SaveChanges();
        }
    }

    public interface IPatientLogic
    {
        void Add(Patient doctor);
        IEnumerable<Patient> GetAll();
        Patient Get(string id);
        void SaveChanges();
    }
}