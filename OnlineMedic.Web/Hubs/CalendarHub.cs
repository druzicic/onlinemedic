﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace OnlineMedic.Web.Hubs
{
    public class CalendarHub : Hub
    {
        public void JoinDoctorCalendar(string doctorId)
        {
            Groups.Add(Context.ConnectionId, doctorId);
        }

        public void SendChatMessage(string who, string message)
        {
            string userId = Context.User.Identity.GetUserId();

            Clients.Group(who).addChatMessage(userId + ": " + message);            
        }

        //public override Task OnConnected()
        //{
        //    string userId = Context.User.Identity.GetUserId();

        //    Groups.Add(Context.ConnectionId, userId);

        //    return base.OnConnected();
        //}
    }
}